package com.logic;



import static org.hamcrest.MatcherAssert.assertThat;

import org.hamcrest.Matcher;

import org.junit.Test;

import com.condrules.Rules;
import com.formsmodels.Habbits;
import com.formsmodels.Person;
import com.logic.RulesTest;

public class RulesTest {

	@SuppressWarnings("unchecked")
	@Test
	public void testApplyBadHabbitsBasedCalcs_NoHabbits() throws Exception {
		double premium = Rules.applyBadHabbitsBasedCalcs(new Person(), 100d);
		assertThat(premium, closeTo(100d, 0.01d));
	}

	private Matcher closeTo(double d, double e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Test
	public void testApplyBadHabbitsBasedCalcs_SomeBadHabbits() throws Exception {
		Person person = new Person();
		person.getHabbits().add(Habbits.Alcohol);
		person.getHabbits().add(Habbits.Smoking);
		double premium = Rules.applyBadHabbitsBasedCalcs(person, 100d);
		assertThat(premium, closeTo(106.09d, 0.01d));
	}

	@Test
	public void testApplyBadHabbitsBasedCalcs_MixOfGoodAndBadHabbits() throws Exception {
		Person person = new Person();
		person.getHabbits().add(Habbits.Alcohol);
		person.getHabbits().add(Habbits.DailyExercise);
		double premium = Rules.applyBadHabbitsBasedCalcs(person, 100d);
		assertThat(premium, closeTo(103d, 0.01d));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testApplyBadHabbitsBasedCalcs_NoBadHabbits() throws Exception {
		Person person = new Person();
		person.getHabbits().add(Habbits.DailyExercise);
		double premium = Rules.applyBadHabbitsBasedCalcs(person, 100d);
		assertThat(premium, closeTo(100d, 0.01d));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testApplyAgeBasedCalcs_LessThan18() throws Exception {
		double premium = Rules.applyAgeBasedCalcs(new Person(), 100d);
		assertThat(premium, closeTo(100d, 0.01d));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testApplyAgeBasedCalcs_WithAgeOf34() throws Exception {
		Person person = new Person();
		person.setAge(34);
		double premium = Rules.applyAgeBasedCalcs(person, 100d);
		assertThat(premium, closeTo(133.1d, 0.01d));
	}

}
