package com.healthstrategies;

import com.formsmodels.Person;

public interface PremiCalcStrategy {

	double calculatePremium(Person person);
}
