package com.formsmodels;

import java.util.HashSet;
import java.util.Set;

public class Person {

	private String name;
	private Gender gender;
	private int age;
	private Set<PreviousHealthIssues> previousHealthIssues;
	private Set<Habbits> habbits;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Set<PreviousHealthIssues> getpreviousHealthIssues() {
		if (previousHealthIssues == null) {
			previousHealthIssues = new HashSet<PreviousHealthIssues>();
		}
		return previousHealthIssues;
	}

	public Set<Habbits> getHabbits() {
		if (habbits == null) {
			habbits = new HashSet<Habbits>();
		}
		return habbits;

	}

}
