package com.healthstrategies;

import java.util.Set;

import com.formsmodels.Gender;
import com.formsmodels.Habbits;
import com.formsmodels.Person;
import com.formsmodels.PreviousHealthIssues;

public class MainCla {

	

	private static void GenderRule() {
		NormalPrCalStrategy strategy= new NormalPrCalStrategy();
		Person person = new Person();
		person.setName("Ramesh");
		person.setGender(Gender.MALE);
		person.setAge(27);
		person.getHabbits().add(Habbits.DailyExercise);

		double premium = strategy.calculatePremium(person);
		System.out.println("GenderRule :"+premium);
		
	}
	private static void BadHabit() {
		NormalPrCalStrategy strategy= new NormalPrCalStrategy();
		Person person = new Person();
		person.setName("Ramesh");
		person.setGender(Gender.MALE);
		person.setAge(27);
		person.getHabbits().add(Habbits.Drugs);
		person.getHabbits().add(Habbits.Smoking);

		double premium = strategy.calculatePremium(person);
		System.out.println("BadHabit :"+premium);
		
	}

	private static void PremiumForNormanGomes() {
		
		NormalPrCalStrategy strategy= new NormalPrCalStrategy();

		Person person = new Person();
		person.setName("Norman Gomes");
		person.setGender(Gender.MALE);
		person.setAge(34);
		
		Set<PreviousHealthIssues> issues = person.getpreviousHealthIssues();
		issues.add(PreviousHealthIssues.Overweight);
		
		Set<Habbits> habbits = person.getHabbits();
		habbits.add(Habbits.Alcohol);
		habbits.add(Habbits.DailyExercise);
		habbits.add(Habbits.Yoga);
		
		
		double ss = strategy.calculatePremium(person);
		System.out.println("Health Insurance Premium for Norman's is:"+ss+" Rs");
		
	}
  //Main Method
	public static void main(String[] args) {
		
		PremiumForNormanGomes();
		GenderRule();
		BadHabit();
		
		
	}
	
}
