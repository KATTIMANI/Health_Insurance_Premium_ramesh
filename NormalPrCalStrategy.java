package com.healthstrategies;

import com.condrules.Rules;
import com.formsmodels.Person;

public class NormalPrCalStrategy implements PremiCalcStrategy{

	
	public double calculatePremium(Person person) {
		// TODO Auto-generated method stub

		double premium = 5000;
		premium = Rules.applyAgeBasedCalcs(person, premium);
		premium = Rules.applyBadHabbitsBasedCalcs(person, premium);
		premium = Rules.applyGenderBasedCalcs(person, premium);
		premium = Rules.applyGoodHabbitsBasedCalcs(person, premium);
		premium = Rules.applyHealthIssuesBasedCalcs(person, premium);		
		return premium;

	}
	

	
}
