package com.logic;


import static org.hamcrest.MatcherAssert.assertThat;


import java.util.Set;

import org.hamcrest.Matcher;
import org.junit.Test;

import com.formsmodels.Gender;
import com.formsmodels.Habbits;
import com.formsmodels.Person;
import com.formsmodels.PreviousHealthIssues;
import com.healthstrategies.NormalPrCalStrategy;
import com.healthstrategies.PremiCalcStrategy;



public class NormalPremStrategy {

	private PremiCalcStrategy strategy = new NormalPrCalStrategy();


	@Test
	public void testCalculatePremium_NormanGomes() throws Exception {
		Person person = new Person();
		person.setName("Norman Gomes");
		person.setGender(Gender.MALE);
		person.setAge(34);
		Set<PreviousHealthIssues> issues = person.getpreviousHealthIssues();
		issues.add(PreviousHealthIssues.Overweight);
		Set<Habbits> habbits = person.getHabbits();
		habbits.add(Habbits.Alcohol);
		habbits.add(Habbits.DailyExercise);

		double premium = strategy.calculatePremium(person);

		
	}

	@Test
	public void testCalculatePremium_goodOldGrandma() throws Exception {
		Person person = new Person();
		person.setName("Ramesh");
		person.setGender(Gender.MALE);
		person.setAge(28);
		person.getHabbits().add(Habbits.DailyExercise);
		person.getHabbits().add(Habbits.Yoga);

		double premium = strategy.calculatePremium(person);

			}

	private Matcher closeTo(double d, double e) {
		// TODO Auto-generated method stub
		return null;
	}
	

	@SuppressWarnings("unchecked")
	@Test
	public void testCalculatePremium_unhealthyKid() throws Exception {
		Person person = new Person();
		person.setName("Satish");
		person.setGender(Gender.MALE);
		person.setAge(23);
		Set<Habbits> habbits = person.getHabbits();
		habbits.add(Habbits.Drugs);
		habbits.add(Habbits.Smoking);
		habbits.add(Habbits.Alcohol);
		Set<PreviousHealthIssues> healthIssues = person.getpreviousHealthIssues();
		healthIssues.add(PreviousHealthIssues.Overweight);

		double premium = strategy.calculatePremium(person);

			}


}
