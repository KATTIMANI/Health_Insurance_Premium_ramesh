package com.formsmodels;

public enum PreviousHealthIssues {

	Hypertension, BloodPressure, BloodSugar, Overweight;
}
