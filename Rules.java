package com.condrules;

import com.formsmodels.Gender;
import com.formsmodels.Habbits;
import com.formsmodels.Person;
import com.formsmodels.PreviousHealthIssues;

public class Rules {

	public static double applyBadHabbitsBasedCalcs(Person person, double premium) {
		
		
		for (Habbits habbit : person.getHabbits()) {
			if (Notchange.badHabbits.contains(habbit)) {
				premium *= 1.03;
			}
		}
		return premium;
	}

	public static double applyGoodHabbitsBasedCalcs(Person person, double premium) {
		
		for (Habbits habbit : person.getHabbits()) {
			if (Notchange.goodHabbits.contains(habbit)) {
				premium *= 0.97;
			}
		}
		return premium;
	}

	public static double applyHealthIssuesBasedCalcs(Person person, double premium) {
		
		for (PreviousHealthIssues issue : person.getpreviousHealthIssues()) {
			premium *= 1.01;
		}
		return premium;
		
	}

	public static double applyGenderBasedCalcs(Person person, double premium) {
		
		if (person.getGender() == Gender.MALE) {
			premium *= 1.02;
		}
		return premium;
	}

	public static double applyAgeBasedCalcs(Person person, double premium) {
		
		if (person.getAge() >= 18) {
			premium *= 1.1;
		}
		if (person.getAge() >= 25) {
			premium *= 1.1;
		}
		if (person.getAge() >= 30) {
			premium *= 1.1;
		}
		if (person.getAge() >= 35) {
			premium *= 1.1;
		}
		if (person.getAge() >= 40) {
			
			int age = person.getAge() - 40;
			while (age >= 5) {
				premium *= 1.2;
				age -= 5;
			}
		}
		return premium;
	}

}
