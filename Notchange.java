package com.condrules;

import java.util.HashSet;
import java.util.Set;

import com.formsmodels.Habbits;

public interface Notchange {
	
	Set<Habbits> goodHabbits = new HashSet<Habbits>() {
		{
			add(Habbits.DailyExercise);
			add(Habbits.Yoga);
			
		}
	};

	Set<Habbits> badHabbits = new HashSet<Habbits>() {
		{
			add(Habbits.Smoking);
			add(Habbits.Alcohol);
			add(Habbits.Drugs);
		}
	};


}
